package fr.poslovitch.staffchat;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.plugin.java.JavaPlugin;

public class StaffChat extends JavaPlugin implements Listener{
	
	@Override
	public void onEnable(){
		getServer().getPluginManager().registerEvents(this, this);
	}
	
	@Override
	public void onDisable(){
		
	}
	
	@EventHandler
	public void onChat(AsyncPlayerChatEvent e){
		Player p = e.getPlayer();
		String msg = e.getMessage();
		
		if(p.hasPermission("staffchat.chat") && msg.startsWith("!")){
			e.setCancelled(true);
			String end = msg.substring(1);
			for(Player staff : Bukkit.getOnlinePlayers()){
				if(staff.hasPermission("staffchat.chat")){
					staff.sendMessage(ChatColor.YELLOW + "STAFF" + ChatColor.GRAY + " | " + ChatColor.GREEN + p.getName() + ChatColor.GRAY + ": " + ChatColor.WHITE + end);
					staff.playSound(staff.getLocation(), Sound.ORB_PICKUP, 1F, 1F);
				}
			}
		}
	}
}
